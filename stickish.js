var stickish = (function($) {

  var stickishObjects = [];

  // Run the sticky logic once initially
  window.requestAnimationFrame(onFrame);

  $(window).scroll(onWindowScroll);
  $(window).resize(onWindowResize);

  function onWindowScroll(event) {
    window.requestAnimationFrame(function() {
      onFrame(false);
    });
  }
  function onWindowResize(event) {
    window.requestAnimationFrame(function() {
      onFrame(true);
    });
  }

  function onFrame(shouldReset) {
    // Main loop
    for(var i=0; i < stickishObjects.length; i++) {
      if(shouldReset) {
        stickishObjects[i].reset();
      }
      stickishObjects[i].update();
    }
  }

  var Stickish = function(options) {
    if(options === undefined || !options.$target) throw "Missing options";

    var
      // Option variables

      /** (Required) Element to make sticky. This will become sticky.
        * (object jQuery) */
      $target     = options.$target,

      /** (Optional) Container element wrapping the nav. This will remain in place.
        * (object jQuery) */
      $container  = options.$container || null,

      /** (Optional) Window edge to attach element to when it is reached.
        * Set 'auto' to enable both top and bottom.
        * (string top|bottom|auto) */
      attachment 	= options.attachment || 'auto',

      /** (Optional) Vertical direction in which container will follow along.
        * Set 'above' to stick to window when the viewport moves above container's original position
        * Set 'below' to stick to window when the viewport moves below container's original position
        * Set 'both' to stick to window in both directions
        * (string above|below|both */
      direction = options.direction || 'below',

      /** (Optional) Set true to add top/bottom styles inline. Otherwise, no top/bottom CSS will be set.
        * (bool) */
      autoPosition 	= (typeof options.autoPosition == "undefined") ? true : options.autoPosition,


      // Internal variables
      currentState = 'static',
      staticOffset = 0, // The original top position/offset of the nav when it's static (not sticky)
      staticHeight // The original height of the nav
      ;


    // Handle deprecated option "toBottom"
    /** (Optional) Set true if Stickish should stick to bottom of window instead of top.
      * Deprecated by option 'attachment'
      * (bool) */
    if(options.toBottom == true) {
      autoPosition = true;
      direction = 'above';
      attachment = 'bottom';
    }

    // If direction is set to both, only "auto" attachment makes sense
    if(direction == 'both') attachment = 'auto';

    stickishObjects.push(this);

    function init() {
      if(!$container) {
        // Wrap $target in a container if no $container was specified
        $container = $target.wrap('<div class="stickish-wrapper">').parent();
      }
      $target.addClass('stickish');

      // Set the container height initially
      setContainerHeight();
    }

    this.reset = function() {
      // Calculate and reset container height
      setContainerHeight();
      this.update();
    }

    this.change = function(options) {
      // Change stickish, updating passed options
      if(!options) return;
      if(options.attachment) attachment = options.attachment;
      if(options.direction) direction = options.direction;
      if(options.autoPosition) autoPosition = options.autoPosition;
      if(direction == 'both') attachment = 'auto';
      this.reset();
    }

    this.update = function() {
      // Check if the static nav offset has changed (i.e content was added on top)
      if(staticOffset !== getStaticOffset()) {
        // if so, store the new offset
        staticOffset = getStaticOffset();
      }

      // Current window top edge position
      var scrollTop = $(window).scrollTop();
      // Current window bottom edge position
      var scrollBottom = scrollTop + $(window).height();
      // Current container top offset
      var offsetTop = staticOffset;
      // Current container bottom offset
      var offsetBottom = staticOffset + staticHeight;

      var state = {
        above: {
          top: scrollBottom > offsetBottom,
          bottom: scrollBottom < offsetBottom
        },
        below: {
          top: scrollTop > offsetTop,
          bottom: scrollBottom > offsetBottom
        }
      }

      state.both = {
        top: state.below.top,
        bottom: state.above.bottom
      }
      state.both.auto = state.both.top || state.both.bottom;

      if(state[direction][attachment]) {
        var attachTo = attachment;
        if(attachment == 'auto') {
          attachTo = state[direction].top ? 'top' : 'bottom';
        }
        setSticky(attachTo);
      } else {
        setStatic();
      }
    }

    function setPosition(_attachment) {
    	if(!autoPosition) return;

    	// Add inline styles to make it stick to top or bottom
      if(_attachment == 'bottom') {
        $target.css({position: 'fixed', top: '', bottom: 0});
      } else if(_attachment == 'top') {
        $target.css({position: 'fixed', top: 0, bottom: ''});
      }
    }
    function unsetPosition() {
    	// Remove the added inline styles
    	$target.css({position: '', top: '', bottom: ''});
    }

    function setSticky(_attachment) {
      currentState = 'sticky';

      // If autoPosition set to false, don't set position styles
      setPosition(_attachment);

      $container.addClass('is-sticky');
      $target.addClass('is-sticky');
    }

    function setStatic() {
      currentState = 'static';

      // If autoPosition set to false, don't set position styles
      if(autoPosition) unsetPosition();
      $container.removeClass('is-sticky');
      $target.removeClass('is-sticky');
    }

    function getStaticOffset() {
      return $container.offset().top;
    }

    function setContainerHeight() {
      // Explicitly set the container height to the same as it's contained navbar
      staticHeight = $target.outerHeight();
      $container.css('height', staticHeight+'px');
    }


    // Initialize when jQuery is ready
    $(init());
  };

  return Stickish;

})(jQuery);

if(module && module.exports) module.exports = sticker;