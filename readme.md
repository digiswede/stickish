Stickish - make things stick
=========
Use Stickish to make elements stick to the browser window when it scrolls.

## Flexibly sticky
Unlike most "sticky" implementations, Stickish can be configured to make elements stick to the top of the window, bottom of the window, or both. 

## Requirements
* jQuery

## Usage
HTML
```
<div class="my-stickish-wrapper"> 
	<div class="my-stickish-element">Being stickish</div>
</div>
```
JS
```
var stickish = new Stickish({
	$target: $('.my-stickish-element')
/* Optional options: 
	$container: jQuery object,
	attachment: "top"|"bottom"|"auto",
	direction: 'above'|'below'|'both',
	autoPosition: true|false
*/
});
```